
/********************************************************************************\
\*  DEC6713_FLASH.c	V2.00													    *\

\*  Copyright 2004 by SEED Electronic Technology LTD.                           *\
\*  All rights reserved. SEED Electronic Technology LTD.                        *\
\*  Restricted rights to use, duplicate or disclose this code are               *\
\*  granted through contract.    											    *\
                                             
\*	Designed by: Hongshuai.Li

\*	Discription: Erase, write and read the whole chip.
\*	Date: Modified	05.10.2005											    *\
\********************************************************************************/

#include <stdio.h>
#include <csl.h>
#include <csl_irq.h>
#include <csl_chip.h>
#include <csl_emif.h>
#include <csl_irq.h>
#include "DEC6713_FLASH.h"
#include <math.h>
#include "TESTDRAM1.h"
/********************************************************************************/

Uint32 i;
Uint32 TempData;
Uint32 Src_StartAdd;
Uint32 Dst_StartAdd;
extern far void vectors();
Uint32 Flash_Erase(Uint32 addr,Uint16 type);
void ProgramFlash (int *sourcepoint, Uint32 flashpoint,unsigned int j);
void Program_One_Word(Uint16 *data,Uint32 addr);
/********************************************************************************/
/********************************************************************************/
void delay(int ttt)//uint 100us
{
	int i,j;
	for(i=0;i<ttt;i++)
	{
		for(j=0;j<2000;j++)	//1716
			asm(" nop ");
	}
}

int main()
	{	
		int flashburn=0;
	   	CSL_init();

	//	InitEmif();
		DEC6713_init();

       // flash_erase();
	    Flash_Erase(0x90000000,0x10);
    	printf("Erase flash ok.\n");

	//	if(flashburn==0)
  	     {
		//  	ProgramFlash (TESTDRAM1, 0x90000000, 0x480);
		//	ProgramFlash (TESTDRAM1, 0x90000000, 0x2104);//hh
			ProgramFlash (TESTDRAM1, 0x90000000, 0x2600);
		//	ProgramFlash (TESTDRAM1, 0x90000000, 0x2000);
 	      }
    	printf("flash is over!");
		return(0);

}/*
void main()
{
	Src_StartAdd = 0x90000000;
	// Initialize CSL, must when using. 
	CSL_init();
	
	// Initialize DEC6713 board. 
	DEC6713_init();
	
	/ Configure interrupt. 
	IRQ_setVecs(vectors);
    IRQ_nmiEnable();
    IRQ_globalEnable();
    
    // Erase flash memory. 
    Flash_Erase(0x90000000,0x10);
	printf("\nErase flash ok.");
	
    // Write flash memory. 
    for(i=0;i<0x40000;i++)
    {
    	Flash_Writes(Src_StartAdd+2*i,fmod(i,0x10000));
    }
    printf("\nWrite flash ok.");
    	
    // Read flash memory. 
    for(i=0;i<0x40000;i++)
    {
    	TempData = Flash_Reads(Src_StartAdd+2*i);
    	if(TempData != fmod(i,0x10000))
    	{
    		printf("\n Testing is Failure!");
    		printf("\nAddress 0x%x is error!",i);
    		exit(0);
    	}
    }
	
	printf("\nOpereation is success.");
}*/
/********************************************************************************\
\*	Flash function difine. *\
\********************************************************************************/
/********************************************************************************\
\* Flash erase function. *\
\********************************************************************************/
Uint32 Flash_Erase(Uint32 addr,Uint16 type)
{
	Uint32 i,j;
	*FLASH_5555 = FLASH_UL1;	//first
	*FLASH_2AAA = FLASH_UL2;	//second
	*FLASH_5555 = FLASH_UL3;	//third
	*FLASH_5555 = FLASH_UL4;
	*FLASH_2AAA = FLASH_UL5;
	switch(type)
	{
		case 0x50:		//block erase
			*(Uint16 *)addr = type;
			while((*(Uint16 *)addr & 0x80) != 0x80);
			for(i = 0; i < BLOCK_SIZE; i++)
			{
				if(*(Uint16 *)(addr + i) != 0xffff)
				{
					j = 0;
					break;
				}
			}
			j = 1;
			break;
		
		case 0x30:		//sector erase
			*(Uint16 *)addr = type;
			while((*(Uint16 *)addr & 0x80) != 0x80);
			for(i = 0; i < SECTOR_SIZE; i++)
			{
				if(*(Uint16 *)(addr + i) != 0xffff)
				{
					j = 0;
					break;
				}
			}
			j = 1;
			break;
			
		case 0x10:		//chip erase
	//	for(;;)
	//	{
			*FLASH_5555 = type;
	//	}
			while((*FLASH_5555 & 0x80) != 0x80);
			for(i = 0; i < CHIP_SIZE; i++)
			{
				if(*(Uint16 *)(addr + i) != 0xffff)
				{
					j = 0;
					break;
				}
			}
			j = 1;
			break;
		
		default:
			break;
	}
	return (j);
}


void ProgramFlash (int *sourcepoint, Uint32 flashpoint,unsigned int j)
{
     unsigned int i;
	 int temp;
	 Uint16 temp1,temp2; 

     for(i=0 ;i< j*4; i+=4)
   //   for(i=0 ;i< 4; i+=4)
     {
       	temp = (*sourcepoint++);
		temp1 = temp & 0xffff;
		temp2 = (temp & 0xffff0000) >> 16;				
		Program_One_Word(&temp1,flashpoint+1*i); 
		Program_One_Word(&temp2,flashpoint+1*(i+2));
  
     } 
}

/********************************************************************************\
\*  Write a single data. *\
\********************************************************************************/
void Program_One_Word(Uint16  *data,Uint32 addr)
{
	//Uint16 TempData=0;
	*FLASH_5555 = FLASH_UL1;
	*FLASH_2AAA = FLASH_UL2;
	*FLASH_5555 = FLASH_PROGRAM;
    //for(;;)
    //{
	*(Uint16 *)addr = *data;
	delay(2);
	//TempData = *(Uint16 *)(addr);
	//}
	//TempData = *(Uint16 *)(addr);
//	while(*(Uint16 *)addr != data);
}

/********************************************************************************\
\* Write the certain length data. *\
\********************************************************************************/
/*void Flash_Writem(Uint32 addr,Uint16 *ptr,Uint32 length)
{
	Uint32 i;
	for(i  = 0; i < length; i++)
	{
	//	for(;;)
	//	{
		Flash_Writes(addr+2*i,*(ptr+i));
	//	}
	}
}
*/
/********************************************************************************\
\* Read a single data. *\
\********************************************************************************/
Uint32 Flash_Reads(Uint32 addr)
{
	return (*(Uint16 *)addr);
}

/********************************************************************************\
\* Read the certain length data. *\
\********************************************************************************/
void Flash_Readm(Uint32 addr,Uint16 *ptr,Uint32 length)
{
	Uint32 i;
	for(i = 0; i < length; i++)
	{
		*(ptr + i) = Flash_Reads(addr+2*i);
	}
}

/********************************************************************************\
\* End of DEC6713_FLASH.C *\
\********************************************************************************/


